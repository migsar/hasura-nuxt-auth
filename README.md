# Hasura as Baas for Nuxt.js

Simple app to demonstrate how to configure Hasura's authentication for use as back-end with Nuxt.js.

You can run the app as a regular Nuxt.js app, you need to have Hasura running for the backend, for that you need to create the Docker image first and then use Docker Compose with `/server/docker-compose.yaml`.
