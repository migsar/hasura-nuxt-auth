import { GetUser } from '../graphql/users'

const initialState = {
  _email: null,
  user: null,
  error: null,
  isLoggedIn: false,
};

export const state = () => initialState;

export const actions = {
  async login({ commit }, email) {
    if (!email) {
      return;
    }

    commit('mailToValidate', email);

    const { defaultClient } = this.app.apolloProvider;

    try {
      const { data } = await defaultClient.query({
        query: GetUser,
        variables: { email },
        fetchPolicy: 'no-cache',
      });

      if (data.users.length === 0) {
        throw new Error('No user found.');
      }

      commit('login', data.users[0]);
      commit('setError', null);
    } catch(error) {
      commit('setError', error.message);
      commit('logout');
    } finally {
      commit('mailToValidate', null);
    }
  },
  logout({ commit }) {
    commit('logout');
  },
};

export const mutations = {
  login(state, user) {
    state.user = user
    state.isLoggedIn = true
  },
  logout(state) {
    state.user = null
    state.isLoggedIn = false
  },
  mailToValidate(state, email) {
    state._email = email;
  },
  setError(state, error) {
    state.error = error;
  },
};
