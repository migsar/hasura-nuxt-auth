import { createHttpLink } from 'apollo-link-http';
import fetch from 'isomorphic-fetch';

export default function ApolloClientConfig(context) {
  const {
    env: { AUTH_SERVER_HEADER_KEY },
    store: { state },
  } = context;

  return {
    defaultHttpLink: false,
    link: createHttpLink({
      uri: process.env.APP_GRAPHQL_ENDPOINT,
      fetch: (uri, options) => {
        const email = state.user?.email || state._email;
        options.headers[AUTH_SERVER_HEADER_KEY] = email;

        return fetch(uri, options);
      },
    }),
  };
}
